package com.dreamslim.nearesttransporttask;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TransportPicker extends AsyncTask<String, Void, Map<String, ArrayList<String>>> {
    private static final String TAG = TransportPicker.class.getSimpleName();
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_SEARCH = "/nearbysearch";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyAAd5iTBMjRt55aDOD4hSpBKXFz7XJo3PI";
    private static final Integer RADIUS = 5000;

    @Override
    protected Map<String, ArrayList<String>> doInBackground(String... params) {
        Map<String, ArrayList<String>> resultMap;

        resultMap = getPlaces(params[0], params[1]);
        if (resultMap != null) {
            return resultMap;
        }
        return null;
    }

    public Map<String, ArrayList<String>> getPlaces(String location, String types) {
        HttpURLConnection connection = null;
        StringBuilder jsonResults = new StringBuilder();
        ArrayList<String> nameList = null;
        ArrayList<String> latitudeList = null;
        ArrayList<String> longitudeList = null;
        Map<String, ArrayList<String>> resultMap = new HashMap<>();

        try {
            StringBuilder stringBuilder = new StringBuilder(PLACES_API_BASE + TYPE_SEARCH + OUT_JSON);
            stringBuilder.append("?key=" + API_KEY);
            stringBuilder.append("&location=").append(location);
            stringBuilder.append("&radius=").append(RADIUS);
            stringBuilder.append("&types=").append(types);
            stringBuilder.append("&rankBy=distance");

            URL url = new URL(stringBuilder.toString());
            connection = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(connection.getInputStream());

            int read;
            char[] buffer = new char[1024];
            while ((read = in.read(buffer)) != -1) {
                jsonResults.append(buffer, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray results = jsonObj.getJSONArray("results");

            nameList = new ArrayList<>(results.length());
            latitudeList = new ArrayList<>(results.length());
            longitudeList = new ArrayList<>(results.length());
            for (int i = 0; i < results.length(); i++) {
                nameList.add(results.getJSONObject(i).getString("name"));
                latitudeList.add(results.getJSONObject(i).getJSONObject("geometry")
                        .getJSONObject("location")
                        .getString("lat"));
                longitudeList.add(results.getJSONObject(i).getJSONObject("geometry")
                        .getJSONObject("location")
                        .getString("lng"));
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
        resultMap.put("name", nameList);
        resultMap.put("latitude", latitudeList);
        resultMap.put("longitude", longitudeList);

        return resultMap;
    }
}
