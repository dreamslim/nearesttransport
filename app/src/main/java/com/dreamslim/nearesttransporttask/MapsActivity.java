package com.dreamslim.nearesttransporttask;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MapsActivity extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap map;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Boolean mRequestingLocationUpdates = true;
    private ArrayList<Boolean> placesTypes = new ArrayList<>(6);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        buildGoogleApiClient();
        createLocationRequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.preference_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        Intent settingsActivity = new Intent(getBaseContext(),
                Preferences.class);
        startActivity(settingsActivity);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        map.clear();
        placesTypes.clear();
        placesTypes.add(0, prefs.getBoolean("checkboxAirport", false));
        placesTypes.add(1, prefs.getBoolean("checkboxBus", true));
        placesTypes.add(2, prefs.getBoolean("checkboxSubway", true));
        placesTypes.add(3, prefs.getBoolean("checkboxTaxi", false));
        placesTypes.add(4, prefs.getBoolean("checkboxTrain", false));
        placesTypes.add(5, prefs.getBoolean("checkboxAuto", false));
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        map.animateCamera(CameraUpdateFactory
                .newLatLngZoom(currentLatLng, 13));

        Map<String, ArrayList<String>> transportPlaces;
        ArrayList<String> nameList;
        ArrayList latitudeList;
        ArrayList longitudeList;
        StringBuilder types = new StringBuilder();
        if (placesTypes.get(0))
            types.append("airport|");
        if (placesTypes.get(1))
            types.append("bus_station|");
        if (placesTypes.get(2))
            types.append("subway_station|");
        if (placesTypes.get(3))
            types.append("taxi_stand|");
        if (placesTypes.get(4))
            types.append("train_station|");
        if (placesTypes.get(5))
            types.append("car_rental");
        else {
            if (types.length() > 0 )
                types.deleteCharAt(types.length() - 1);
        }

        if (types.length() > 0) {
            StringBuilder stringLocation = new StringBuilder().append(location.getLatitude()).append(",").append(location.getLongitude());
            try {
                transportPlaces = new TransportPicker().execute(stringLocation.toString(), types.toString()).get();

                if (transportPlaces != null) {
                    nameList = transportPlaces.get("name");
                    latitudeList = transportPlaces.get("latitude");
                    longitudeList = transportPlaces.get("longitude");
                    for (int i = 0; i < nameList.size(); i++) {
                        map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitudeList.get(i).toString()), Double.parseDouble(longitudeList.get(i).toString())))
                                .title(nameList.get(i)));
                    }
                }
            } catch (InterruptedException ex) {
                Log.e(TAG, "Error with TransportPicker interrupt", ex);
            } catch (ExecutionException ex) {
                Log.e(TAG, "Error with TransportPicker execute", ex);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            final Place place = PlacePicker.getPlace(data, this);
            final CharSequence name = place.getName();
            final LatLng latLng = place.getLatLng();
            final List types = place.getPlaceTypes();

            if (types.contains(Place.TYPE_AIRPORT) ||
                    types.contains(Place.TYPE_TRAIN_STATION) ||
                    types.contains(Place.TYPE_TAXI_STAND) ||
                    types.contains(Place.TYPE_SUBWAY_STATION)) {
                map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(name.toString()));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(4000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

        if (mGoogleApiClient != null) {
            PendingResult<PlaceLikelihoodBuffer> result =
                    Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);

            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                    likelyPlaces.release();
                }
            });
        }
    }
}

